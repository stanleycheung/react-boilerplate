import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { createStore } from 'redux';
import { Provider } from 'react-redux'
import combineReducers from './Data/Reducer';

import RouterSetting from './Router';

injectTapEventPlugin();

// Reduce
let store = createStore(combineReducers);
store.subscribe(
  () => console.log("STORE LOG", store.getState())
)

// Index component
class Index extends Component {
  render() {
    return (
        <MuiThemeProvider>
          <RouterSetting/>
        </MuiThemeProvider>
    );
  }
}


ReactDOM.render(
  <Index />,
  document.getElementById('root')
);
