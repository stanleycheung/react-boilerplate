import { ApiPath } from '../../../Config';

export default async function GetClasses() {
  fetch(`${ApiPath}/classes`, {
    method: 'GET'
  })
}
