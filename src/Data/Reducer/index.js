import { combineReducers } from 'redux'

const localStore = false;

function general(state={}, action){
  switch (action.type) {
    default:
      return state;
  }
}

const cr = combineReducers({
  general,
})

export default cr;
